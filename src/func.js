const getSum = (str1, str2) => {
    if (typeof (str1) !== "string" || typeof (str2) !== "string") {
        return false;
    }
    if (str1.trim().length === 0) {
        return str2
    }
    if (str2.trim().length === 0) {
        return str1
    }

    if (/^[a-zA-Z ]*$/.test(str1) || /^[a-zA-Z]*$/.test(str2)) {
        return false;
    }

    let firstNumber = BigInt(str1)
    let secondNumber = BigInt(str2)
    let result = firstNumber + secondNumber;
    return result.toString();

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let statistics = {Post: 0, comments: 0}

    statistics.Post = listOfPosts.filter(s => s.author === authorName).length
    let f = (listOfPosts.map(s => s.comments).filter(c => c !== undefined)).flat().filter(a => a.author === authorName)
    statistics.comments = f.length
    return `Post:${statistics.Post},comments:${statistics.comments}`
};

const tickets = (people) => {

    let casa = 0;
    for (let m of people) {
        if (m > 25) {
            casa -= m - 25;
        }
        if (casa < 0) {
            break;
        } else {
            casa += 25;
        }

    }

    return casa < 0 ? "NO" : "YES"
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
